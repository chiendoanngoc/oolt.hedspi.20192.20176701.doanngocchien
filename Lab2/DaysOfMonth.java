import java.util.Scanner;
public class DaysOfMonth {

	public static void main(String args[]){
		Scanner month = new Scanner(System.in);
		Scanner year = new Scanner(System.in);
		System.out.print("Year= ");
		int Year = year.nextInt();
		while (Year < 1)
		{
			System.out.println("Invalid year!");
			System.out.print("Year= ");
			Year = year.nextInt();
		}
		System.out.print("Month= ");
		int Month = month.nextInt();
		while ((Month < 1) || (Month > 12))
		{
			System.out.println("Invalid month!");
			System.out.print("Month= ");
			Month = month.nextInt();
		}
		
		int days;
		if ((Month == 1) || (Month == 3) || (Month == 5) || (Month == 7) || (Month == 8) || (Month == 10) || (Month == 12))
			days = 30;
		else if ((Month == 4) || (Month == 6) || (Month == 9) || (Month == 11))
			days = 31;
		else{
			if (Year % 4 == 0)
				days = 29;
			else
				days = 28;
		}
		System.out.println("Days= " + days);
	}
}

package hust.soict.hedspi.aims.order.Order;
import java.lang.Math;
import java.util.ArrayList;
import hust.soict.hedspi.aims.media.*;


public class Order {
	
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;
	private static int nbOrders = 0;
	
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	
	public Order() {
		super();
		if(nbOrders >= MAX_LIMITTED_ORDERS) {
			System.out.println("Cannot create more order.");
		}
		else {
			nbOrders++;
			System.out.println("New order was created.");
		}
	}
	
	public void addMedia(Media item) {
		if(itemsOrdered.size() < MAX_NUMBERS_ORDERED) {
			itemsOrdered.add(item);
			System.out.println("Item has been added.");
		}
		else
			System.out.println("The order is already full.");
	}
	
	public void removeMedia(Media item) {
		if (itemsOrdered.contains(item) == false)
			System.out.println("Cannot find item.");
		else {
			itemsOrdered.remove(item);
			System.out.println("Item removed.");
		}
	}
	
	public void addBook(String title, String category, double cost, String id) {
		if(itemsOrdered.size() < MAX_NUMBERS_ORDERED) {
			Book item = new Book();
			item.setCategory(category);
			item.setCost(cost);
			item.setId(id);
			item.setTitle(title);
			itemsOrdered.add(item);
			System.out.println("Book has been added.");
		}
		else
			System.out.println("The order is already full.");
	}
	
	public void addDisc(String title, String category, double cost, String id) {
		if(itemsOrdered.size() < MAX_NUMBERS_ORDERED) {
			Disc item = new Disc();
			item.setCategory(category);
			item.setCost(cost);
			item.setId(id);
			item.setTitle(title);
			itemsOrdered.add(item);
			System.out.println("Disc has been added.");
		}
		else
			System.out.println("The order is already full.");
	}
	
	public void removeMedia(String id) {
		boolean flag = true;
		for (Media item : itemsOrdered) {
			if (item.getId().equals(id)) {
				itemsOrdered.remove(item);
				System.out.println("Item removed.");
				flag = false;
				break;
			}
		}
		if (flag)
			System.out.println("Cannot find item.");
	}

	public double totalCost() {
		double total = 0;
		for(Media item : itemsOrdered) {
			total += item.getCost();
		}
		return total;
	}
	
	public void print_order() {
		System.out.println("*********************Order************************");
		System.out.println("Ordered Items:");
		for(int i=0;i<itemsOrdered.size();i++) {
			Media item = itemsOrdered.get(i);
			System.out.println((i+1)+". " + item.getTitle() + " - " + item.getCategory() + " - id " + item.getId() + ": " + item.getCost() + "$");
		}
		System.out.println("Total cost: " + totalCost() + "$");
		System.out.println("**************************************************");
	}
	
	public Media getALuckyItem() {
		int max = itemsOrdered.size() - 1; 
        int min = 0; 
        int range = max - min + 1;
        int rand = (int)(Math.random() * range) + min;
        itemsOrdered.get(rand).setCost(0);
        return itemsOrdered.get(rand);
	}
	
}












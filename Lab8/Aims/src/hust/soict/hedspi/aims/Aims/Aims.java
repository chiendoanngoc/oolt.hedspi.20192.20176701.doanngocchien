package hust.soict.hedspi.aims.Aims;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Disc;
import hust.soict.hedspi.aims.order.Order.Order;

public class Aims {
	
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
		}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	/*	Scanner sc = new Scanner(System.in);
		int choose;
		Order anOrder;
		List<Order> list = new ArrayList<Order>();
		do {
			showMenu();
			choose = sc.nextInt();
			sc.nextLine();
			switch (choose) {
			case 1:
				anOrder = new Order();
				list.add(0,anOrder);
				System.out.println("\n");
				break;
			case 2:
				int choose_type;
				String title, category, id, bool;
				double cost;
				do {
					System.out.println("1. Book");
					System.out.println("2. CompactDisc");
					System.out.println("3. DigitalVideoDisc");
					System.out.println("Please choose a number: 1-2-3");
					choose_type = sc.nextInt();
					sc.nextLine();
					switch (choose_type) {
					case 1:
						System.out.println("Title: ");
						title = sc.nextLine();
						System.out.println("Category: ");
						category = sc.nextLine();
						System.out.println("Cost: ");
						cost = sc.nextDouble();
						sc.nextLine();
						System.out.println("id: ");
						id = sc.nextLine();
						list.get(0).addBook(title, category, cost, id);
						System.out.println("\n");
						break;
					case 2:
						System.out.println("Title: ");
						title = sc.nextLine();
						System.out.println("Category: ");
						category = sc.nextLine();
						System.out.println("Cost: ");
						cost = sc.nextDouble();
						sc.nextLine();
						System.out.println("id: ");
						id = sc.nextLine();
						DigitalVideoDisc disc = new DigitalVideoDisc(title, category);
						list.get(0).addMedia(disc);
						System.out.println("Play? Yes or No?");
						bool = sc.nextLine();
						if (bool.equals("Yes")) {
							disc.play();
						}
						System.out.println("\n");
						break;
					case 3:
						System.out.println("Title: ");
						title = sc.nextLine();
						System.out.println("Category: ");
						category = sc.nextLine();
						System.out.println("Cost: ");
						cost = sc.nextDouble();
						sc.nextLine();
						System.out.println("id: ");
						id = sc.nextLine();
						DigitalVideoDisc dvd = new DigitalVideoDisc(title, category);
						list.get(0).addMedia(dvd);
						System.out.println("Play? Yes or No?");
						bool = sc.nextLine();
						if (bool.equals("Yes")) {
							dvd.play();
						}
						System.out.println("\n");
						break;
					default:
						System.out.println("Wrong number. Please choose again!");
						break;
					}
				} while ((choose_type < 1) || (choose_type > 3));
				break;
			case 3:
				System.out.println("id: ");
				String remove_id = sc.nextLine();
				list.get(0).removeMedia(remove_id);
				System.out.println("\n");
				break;
			case 4:
				list.get(0).print_order();
				System.out.println("\n");
				break;
			case 0:
				sc.close();
				System.out.println("Good bye!");
				break;
			default:
				System.out.println("Wrong number. Please choose again.\n");
				break;
			}
			
			
		} while (choose != 0); */
		
		Collection collection = new ArrayList();
		
		// Create a new DVD object and set the fields
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		
		collection.add(dvd2);
		collection.add(dvd1);
		collection.add(dvd3);
		
		// Iterate through the ArrayList and output their titles
		// (unsorted order)
		Iterator iterator = collection.iterator();
		System.out.println("---------------------------------------");
		System.out.println("The DVDs currently in the order are: ");
		
		while (iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
		}
		
		// Sort the collection of DVDs - based on compareTo() method
		java.util.Collections.sort((List)collection);
		
		// Iterate through the ArrayList and output their titles - in sorted order
		iterator = collection.iterator();
		System.out.println("---------------------------------------");
		System.out.println("The DVDs in sorted order are: ");
		
		while (iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
		}
		
		System.out.println("---------------------------------------");
		
		
		
		
	}
	

}
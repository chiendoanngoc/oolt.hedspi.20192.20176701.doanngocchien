package hust.soict.hedspi.aims.media;
import java.util.ArrayList;

public class CompactDisc 
	extends Disc implements Playable, Comparable {
	private String artist;
	private ArrayList<Track> tracks = new ArrayList<Track>();
	
	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	public void addTrack (String title, int length) {
		Track track = new Track(title, length);
		if (tracks.contains(track) == true)
			System.out.println("Track already in.");
		else {
			tracks.add(track);
			System.out.println("Track added.");
		}
	}
	
	public void removeTrack(String title, int length) {
		Track track = new Track(title, length);
		if (tracks.contains(track) == false)
			System.out.println("Cannot find track.");
		else {
			tracks.remove(track);
			System.out.println("Track removed.");
		}
	}
	
	public int getLength() {
		int length = 0;
		for (Track track : tracks) {
			length += track.getLength();
		}
		return length;
	}

	public CompactDisc() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing disc: " + this.getTitle() + " of " + this.getArtist());
		System.out.println("Disc length: " + this.getLength());
		for (Track track : tracks) {
			track.play();
		}
	}

	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

}

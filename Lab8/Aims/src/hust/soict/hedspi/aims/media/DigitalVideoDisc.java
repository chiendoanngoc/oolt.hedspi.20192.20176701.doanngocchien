package hust.soict.hedspi.aims.media;

public class DigitalVideoDisc 
	extends Disc implements Playable, Comparable {
	public DigitalVideoDisc(String title) {
		super();
		this.title = title;
	}
	public DigitalVideoDisc(String title, String category) {
		super();
		this.title = title;
		this.category = category;
	}
	
	public boolean search(String title) {
		this.title = this.title.toLowerCase();
		title = title.toLowerCase();
		String [] split_title = title.strip().split(" ");
		for (String a : split_title)
			if (this.title.contains(a) == false)
				return false;
		return true;
	}
	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}

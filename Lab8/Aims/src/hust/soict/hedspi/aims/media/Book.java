package hust.soict.hedspi.aims.media;
import java.util.ArrayList;
import java.util.List;

public class Book extends Media implements Comparable {
	private List<String> authors = new ArrayList<String>();
	
	public void addAuthor(String authorName) {
		if (authors.contains(authorName) == true)
			System.out.println("Author already added.");
		else {
			authors.add(authorName);
			System.out.println("Author added.");
		}
	}
	
	public void removeAuthor(String authorName) {
		if (authors.contains(authorName) == false)
			System.out.println("Cannot find author.");
		else {
			authors.remove(authorName);
			System.out.println("Author removed.");
		}
	}

	public List<String> getAuthors() {
		return authors;
	}


	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}


	public Book() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

}

package hust.soict.hedspi.date;

public class Aims {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Order anOrder = new Order();
		anOrder.setDateOrdered("01-04-2020");
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("roger allers");
		dvd1.setLength(87);
		
//		anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("george lucas");
		dvd2.setLength(124);
//		anOrder.addDigitalVideoDisc(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Blood Wars");
		dvd3.setCategory("Science fiction");
		dvd3.setCost(20.95f);
		dvd3.setDirector("george lucas");
		dvd3.setLength(124);
		
		DigitalVideoDisc dvd_list[] = {dvd1, dvd2, dvd3};
		anOrder.addDigitalVideoDisc(dvd_list);
//		anOrder.removeDigitalVideoDisc(dvd3);
		
		Order anotherOrder = new Order();
		anotherOrder.addDigitalVideoDisc(dvd1);
		System.out.println("total cost :");
		System.out.println(anOrder.totalCost());
		System.out.println(dvd1.getCost());
		System.out.println(dvd2.getCost());
		System.out.println(anOrder.getDateOrdered());
		anOrder.OrderDetail();
		
	}

}

package hust.soict.hedspi.aims.Aims;

import java.util.Scanner;

import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.order.Order;

public class Aims {
	public static void showMenu() {
		
		int choiceentry = -1;
		Scanner scanchoice = new Scanner(System.in);
		int currOrder_id = -1;
	    Order currOrder = new Order();
	    while(1==1){

	    	System.out.println("Order Management Application: ");
			System.out.println("--------------------------------");
			System.out.println("1. Create new order");
			System.out.println("2. Add item to the order");
			System.out.println("3. Delete item by id");
			System.out.println("4. Display the items list of order");
			System.out.println("0. Exit");
			System.out.println("--------------------------------");
			System.out.println("Please choose a number: 0-1-2-3-4");
	        if(scanchoice.hasNextInt())
	        choiceentry = scanchoice.nextInt();
	        switch(choiceentry){
	        case 1:
	        	currOrder_id = 123;
	        	currOrder = new Order(currOrder_id);
	        	
	        	currOrder.setDateOrdered("01-04-2020");
	    		break;
	        case 2:
	        	System.out.println("Enter item: ");
	        	DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
	    		dvd1.setCategory("Animation");
	    		dvd1.setCost(19.95f);
	    		dvd1.setDirector("roger allers");
	    		dvd1.setLength(87);
	    		dvd1.setId(123);
	    		currOrder.addMedia(dvd1);
	        	break;
	        case 3:
	        	currOrder.removeMedia(123);
	        	break;
	        case 4:
	        	currOrder.OrderDetail();
		        break;
	        case 0:
		        return;
		    default:
		    	System.out.println("Error!");
	   }

	    }
	    
	     
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		showMenu();
		Order anOrder = new Order();
		anOrder.setDateOrdered("01-04-2020");
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("roger allers");
		dvd1.setLength(87);
//		if (dvd1.search("Lion")==true)
//			System.out.println("ok");
		
//		anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("george lucas");
		dvd2.setLength(124);
//		anOrder.addDigitalVideoDisc(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Blood Wars");
		dvd3.setCategory("Science fiction");
		dvd3.setCost(20.95f);
		dvd3.setDirector("george lucas");
		dvd3.setLength(124);
		
		DigitalVideoDisc dvd_list[] = {dvd1, dvd2, dvd3};
		anOrder.addMedia(dvd_list);
//		anOrder.removeDigitalVideoDisc(dvd3);
		
		Order anotherOrder = new Order();
		anotherOrder.addMedia(dvd1);
		System.out.println("total cost :");
		System.out.println(anOrder.totalCost());
		System.out.println(dvd1.getCost());
		System.out.println(dvd2.getCost());
		System.out.println(anOrder.getDateOrdered());
//		anOrder.OrderDetail();
		anOrder.getALuckyItem();
		
	}

}

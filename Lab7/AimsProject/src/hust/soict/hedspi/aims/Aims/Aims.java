package hust.soict.hedspi.aims.Aims;

import java.util.Scanner;

import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Track;
import hust.soict.hedspi.aims.order.Order;
import hust.soict.hedspi.aims.order.Test.MemoryDeamon;

public class Aims {
	private static Scanner sc = new Scanner(System.in);
	private static Order anOrder;
	
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}

	public static void addItem() {
		// test
		// Media newItem = new Media("title", "category", 1, 12);
		int choice;
		
		do {
			System.out.println("1. Book");
			System.out.println("2. CompactDisc");
			System.out.println("3. DigitalVideoDisc");
			System.out.println("--------------------------------");
			System.out.println("Please choose a number: 1-2-3");
			choice = sc.nextInt();
			sc.nextLine();
			switch (choice) {
			case 1:
				Book book1 = new Book("Sherlock Holmes", "Conan", 123);
				anOrder.addMedia(book1);
				break;
			case 2:
				CompactDisc disc1 = new CompactDisc("Steve", 11);
				Track track1 = new Track();
				track1.setTitle("Unravel");
				track1.setLength(12);
				Track track2 = new Track();
				track1.setTitle("My heart will go on");
				track1.setLength(12);
				disc1.addTrack(track1);
				disc1.addTrack(track2);
				anOrder.addMedia(disc1);
				
				//ask for play
				System.out.println("Do you want to play this track?(Yes/No)");
				String answer = sc.next();
				if(answer.equals( "yes")) {
					disc1.play();
				}
				break;
			case 3:
				DigitalVideoDisc disc2 = new DigitalVideoDisc("ss", "ss", "ss");
				anOrder.addMedia(disc2);
				break;
			default:
				break;
			}
		} while (choice > 3 || choice < 1);
		
		
	}

	public static void removeItem() {
		System.out.print("Enter the id: ");
		anOrder.removeMedia(sc.nextInt());
		sc.nextLine();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n;
		MemoryDeamon Deamon = new MemoryDeamon(); 
		Thread thread1 = new Thread(Deamon);
		thread1.setDaemon(true);
		thread1.start();
		
		while (true) {
			showMenu();
			n = sc.nextInt();
			sc.nextLine();
			switch (n) {
			case 1:
				anOrder = new Order();
				break;
			case 2:
				addItem();
				break;
			case 3:
				removeItem();
				break;
			case 4:
				anOrder.OrderDetail();
				break;
			case 0:
				sc.close();
				System.exit(0);
			default:
				break;
			}
		}
		
	}
//	public static void showMenu() {
//		
//		int choiceentry = -1;
//		Scanner scanchoice = new Scanner(System.in);
//		int currOrder_id = -1;
//	    Order currOrder = new Order();
//	    while(1==1){
//
//	    	System.out.println("Order Management Application: ");
//			System.out.println("--------------------------------");
//			System.out.println("1. Create new order");
//			System.out.println("2. Add item to the order");
//			System.out.println("3. Delete item by id");
//			System.out.println("4. Display the items list of order");
//			System.out.println("0. Exit");
//			System.out.println("--------------------------------");
//			System.out.println("Please choose a number: 0-1-2-3-4");
//	        if(scanchoice.hasNextInt())
//	        choiceentry = scanchoice.nextInt();
//	        switch(choiceentry){
//	        case 1:
//	        	currOrder_id = 123;
//	        	currOrder = new Order(currOrder_id);
//	        	
//	        	currOrder.setDateOrdered("01-04-2020");
//	    		break;
//	        case 2:
//	        	System.out.println("Enter item: ");
//	        	DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
//	    		dvd1.setCategory("Animation");
//	    		dvd1.setCost(19.95f);
//	    		dvd1.setDirector("roger allers");
//	    		dvd1.setLength(87);
//	    		dvd1.setId(123);
//	    		currOrder.addMedia(dvd1);
//	        	break;
//	        case 3:
//	        	currOrder.removeMedia(123);
//	        	break;
//	        case 4:
//	        	currOrder.OrderDetail();
//		        break;
//	        case 0:
//		        return;
//		    default:
//		    	System.out.println("Error!");
//	   }
//
//	    }
//	    
//	     
//	}
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		showMenu();
//		Order anOrder = new Order();
//		anOrder.setDateOrdered("01-04-2020");
//		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
//		dvd1.setCategory("Animation");
//		dvd1.setCost(19.95f);
//		dvd1.setDirector("roger allers");
//		dvd1.setLength(87);
////		if (dvd1.search("Lion")==true)
////			System.out.println("ok");
//		
////		anOrder.addDigitalVideoDisc(dvd1);
//		
//		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
//		dvd2.setCategory("Science fiction");
//		dvd2.setCost(24.95f);
//		dvd2.setDirector("george lucas");
//		dvd2.setLength(124);
////		anOrder.addDigitalVideoDisc(dvd2);
//		
//		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Blood Wars");
//		dvd3.setCategory("Science fiction");
//		dvd3.setCost(20.95f);
//		dvd3.setDirector("george lucas");
//		dvd3.setLength(124);
//		
//		DigitalVideoDisc dvd_list[] = {dvd1, dvd2, dvd3};
//		anOrder.addMedia(dvd_list);
////		anOrder.removeDigitalVideoDisc(dvd3);
//		
//		Order anotherOrder = new Order();
//		anotherOrder.addMedia(dvd1);
//		System.out.println("total cost :");
//		System.out.println(anOrder.totalCost());
//		System.out.println(dvd1.getCost());
//		System.out.println(dvd2.getCost());
//		System.out.println(anOrder.getDateOrdered());
////		anOrder.OrderDetail();
//		anOrder.getALuckyItem();
//		
//	}

}

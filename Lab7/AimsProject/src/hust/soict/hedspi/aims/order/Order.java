package hust.soict.hedspi.aims.order;

import java.util.ArrayList;
import java.util.Random;

import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;

public class Order {
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public static final int MAX_NUMBERS_ORDERED = 10;
	
	public static final int MAX_LIMITTED_ORDERS = 2;
	private static int nbOrder = 0;
	public Order(int id) {
		this.id = id;
	}
	public Order(){
		nbOrder++;
		if (nbOrder > MAX_LIMITTED_ORDERS) {
			System.out.println("Order Limitted!");
		}
	}
	
	private float sum = 0;
	private String  dateOrdered = "nn-tt-nnnn";
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	public String getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	public void addMedia(Media disc) {
		if (nbOrder > MAX_LIMITTED_ORDERS) {
			System.out.println("Order Limitted! Can't add item");
			return;
		}
		if(itemsOrdered.size() < MAX_NUMBERS_ORDERED) {
			
			itemsOrdered.add(disc);
			int size = itemsOrdered.size() - 1;
			sum = sum + itemsOrdered.get(size).getCost();
			System.out.println("the disc has been added to the order.");
		} else {
			System.out.println("the order is full");
		}
	}
	public void addMedia(Media disc1,
									Media disc2) {
		if (nbOrder > MAX_LIMITTED_ORDERS) {
			System.out.println("Order Limitted! Can't add item");
			return;
		}
		if(itemsOrdered.size() < MAX_NUMBERS_ORDERED) {
			
			itemsOrdered.add(disc1);
			int size = itemsOrdered.size() - 1;
			sum = sum + itemsOrdered.get(size).getCost();
			
			System.out.println("the disc has been added to the order.");
		} else {
			System.out.println("the order is full");
		}
		if(itemsOrdered.size() < MAX_NUMBERS_ORDERED) {
					
			itemsOrdered.add(disc2);
			int size = itemsOrdered.size() - 1;
			sum = sum + itemsOrdered.get(size).getCost();
					
			System.out.println("the disc has been added to the order.");
		} else {
			System.out.println("the order is full");
		}
	}
	public void addMedia(Media [] dvdList) {
		if (nbOrder > MAX_LIMITTED_ORDERS) {
			System.out.println("Order Limitted! Can't add item");
			return;
		}
		int list_length = dvdList.length;
		
			
			for (int i = 0; i < list_length; i++) {
				if(itemsOrdered.size() < MAX_NUMBERS_ORDERED) {
					
					itemsOrdered.add(dvdList[i]);
					int size = itemsOrdered.size() - 1;
					sum = sum + itemsOrdered.get(size).getCost();
					
					System.out.println("the disc has been added to the order.");
				}
				else {
					System.out.println("the order is full, cannot add " + dvdList[i].getTitle());
			}
//			itemsOrdered[qtyOrdered] = disc;
//			qtyOrdered = qtyOrdered + list_length;
			
		} 
	}
	public void removeMedia(Media disc) {
//		int i,j,m;
//		m=0;
//		for(i=0;i<itemsOrdered.size();i++) {
//			if(itemsOrdered[i] == disc) {
//				m=1;
//				break;
//			}
//		}
//		if(m == 0) {
//			System.out.println("Not found the disc./n");
//		}
//		else {
//		for(j=i;j<qtyOrdered;j++) {
//			itemsOrdered[j] = itemsOrdered[j+1];
//		}
//		qtyOrdered -=1;
//		}
		
		if (itemsOrdered.contains(disc)) {
			itemsOrdered.remove(disc);
		} else {
			System.out.println("Not found the disc./n");
		}
	}
	public void removeMedia(int id) {
		for(int i = 0; i<itemsOrdered.size(); i++) {
			if (itemsOrdered.get(i).getId() == id) {
				itemsOrdered.remove(itemsOrdered.get(i));
				return;
			}		
		}
	}
	public float totalCost() {
//		float sum = 0;
//		for(int i = 0;i<qtyOrdered; i++) {
//			sum = sum + itemsOrdered[i].getCost();
//		}
		return sum;
	}
	
	public void OrderDetail() {
		System.out.println("*********************Order************************");
		for (int i=0; i<itemsOrdered.size(); i++) {
			System.out.println("1. " +  itemsOrdered.get(i).getTitle()
							+ " - " + itemsOrdered.get(i).getCategory()
							+ ": " + itemsOrdered.get(i).getCost() + "$");
		}
		
		System.out.println("**************************************************");
	}
	public void getALuckyItem() {
		Random rand = new Random();
		int rand_int = rand.nextInt(itemsOrdered.size());
//		DigitalVideoDisc FreeItem = itemsOrdered.get(rand_int);
		System.out.println("Free lucky Item: "+ itemsOrdered.get(rand_int).getTitle());
		sum = sum - itemsOrdered.get(rand_int).getCost();
		itemsOrdered.get(rand_int).setTitle(itemsOrdered.get(rand_int).getTitle() + "***");
		itemsOrdered.get(rand_int).setCost(0);
		
		
		
	}
	
}

package hust.soict.hedspi.date;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 2;
	
	public static final int MAX_LIMITTED_ORDERS = 1;
	private static int nbOrder = 0;
	Order(){
		nbOrder++;
		if (nbOrder > MAX_LIMITTED_ORDERS) {
			System.out.println("Order Limitted!");
		}
	}
	
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered = 0;
	private String  dateOrdered = "nn-tt-nnnn";

	
	public String getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (nbOrder > MAX_LIMITTED_ORDERS) {
			System.out.println("Order Limitted! Can't add item");
			return;
		}
		if(qtyOrdered < MAX_NUMBERS_ORDERED) {
			
			itemsOrdered[qtyOrdered] = disc;
			qtyOrdered = qtyOrdered+1;
			System.out.println("the disc has been added to the order.");
		} else {
			System.out.println("the order is full");
		}
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc1,
									DigitalVideoDisc disc2) {
		if (nbOrder > MAX_LIMITTED_ORDERS) {
			System.out.println("Order Limitted! Can't add item");
			return;
		}
		if(qtyOrdered < MAX_NUMBERS_ORDERED) {
			
			itemsOrdered[qtyOrdered] = disc1;
			qtyOrdered = qtyOrdered+1;
			System.out.println("the disc has been added to the order.");
		} else {
			System.out.println("the order is full");
		}
		if(qtyOrdered < MAX_NUMBERS_ORDERED) {
					
					itemsOrdered[qtyOrdered] = disc2;
					qtyOrdered = qtyOrdered+1;
					System.out.println("the disc has been added to the order.");
				} else {
					System.out.println("the order is full");
				}
	}
	public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList) {
		if (nbOrder > MAX_LIMITTED_ORDERS) {
			System.out.println("Order Limitted! Can't add item");
			return;
		}
		int list_length = dvdList.length;
		
			
			for (int i = 0; i < list_length; i++) {
				if(qtyOrdered < MAX_NUMBERS_ORDERED) {
					itemsOrdered[qtyOrdered] = dvdList[i];
					qtyOrdered++;
					System.out.println("the disc has been added to the order.");
				}
				else {
					System.out.println("the order is full, cannot add " + dvdList[i].getTitle());
			}
//			itemsOrdered[qtyOrdered] = disc;
//			qtyOrdered = qtyOrdered + list_length;
			
		} 
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int i,j,m;
		m=0;
		for(i=0;i<qtyOrdered;i++) {
			if(itemsOrdered[i] == disc) {
				m=1;
				break;
			}
		}
		if(m == 0) {
			System.out.println("Not found the disc./n");
		}
		else {
		for(j=i;j<qtyOrdered;j++) {
			itemsOrdered[j] = itemsOrdered[j+1];
		}
		qtyOrdered -=1;
	}
	}
	public float totalCost() {
		float sum = 0;
		for(int i = 0;i<qtyOrdered; i++) {
			sum = sum + itemsOrdered[i].getCost();
		}
		return sum;
	}
	
	public void OrderDetail() {
		System.out.println("*********************Order************************");
		for (int i=0; i<qtyOrdered; i++) {
			System.out.println("1. " +  itemsOrdered[i].getTitle() + " - " + itemsOrdered[i].getCategory() + " - " + itemsOrdered[i].getDirector() + " - " + itemsOrdered[i].getLength() + ": " + itemsOrdered[i].getCost() + "$");
		}
		
		System.out.println("**************************************************");
	}
	
}
